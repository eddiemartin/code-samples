<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
</head>
<link rel="stylesheet" href="../css/estilo.css" />
<script type="text/javascript" charset="UTF-8" src="../jscripts/jquery-1.8.0.min.js"></script>
<script type="text/javascript" charset="UTF-8" src="../jscripts/jquery-ui-1.8.23.custom.min.js"></script>
<%	//final String SEPARADOR = "\\";
	java.util.Calendar cl = java.util.Calendar.getInstance();
	int anio = cl.get(java.util.Calendar.YEAR);
	
	System.out.println("*** Carga de archivos");
	
	//Carga de archivos al servidor.
	java.io.File file = null;
	int maxFileSize = 10000 * 1024;
	int maxMemSize = 10000 * 1024;
	ServletContext context = pageContext.getServletContext();

	// Verify the content type
	String contentType = request.getContentType();

	System.out.println(contentType);

	String html = "";
	
	if (contentType.indexOf("multipart/form-data") >= 0 || contentType.indexOf("multipart/mixed") >= 0) {

		org.apache.commons.fileupload.disk.DiskFileItemFactory factory = new org.apache.commons.fileupload.disk.DiskFileItemFactory();
		// maximum size that will be stored in memory
		factory.setSizeThreshold(maxMemSize);
		// Location to save data that is larger than maxMemSize.
		factory.setRepository(new java.io.File("c:\\temp"));

		// Create a new file upload handler
		org.apache.commons.fileupload.servlet.ServletFileUpload upload = new org.apache.commons.fileupload.servlet.ServletFileUpload(factory);
		// maximum file size to be uploaded.
		upload.setSizeMax(maxFileSize);
		
		try {
			// Parse the request to get file items.
			java.util.List<?> fileItems = upload.parseRequest(request);

			// Process the uploaded file items
			java.util.Iterator<?> i = fileItems.iterator();

			boolean isInMemory = false;
			long sizeInBytes = 0;
			org.apache.commons.fileupload.FileItem fi = null; 
			org.apache.commons.fileupload.FileItem fi2 = null;
			int archivo = 0;
			int siniestro = 0;
			String fieldName = "";
			String fileName = "";
			String filePath = "";
			String urlPath = "";
			String mensaje = "";
			boolean val = true;
			
			while (i.hasNext()) {
				fi = (org.apache.commons.fileupload.FileItem) i.next();
				if (fi.isFormField()) {
					if (fi.getFieldName().equals("ID_Siniestro") && util.ObtieneInt(fi.getString()) != 0) {
						siniestro = util.ObtieneInt(fi.getString());
						System.out.println("******** " + siniestro);
					}
				} 
			} 

			i = fileItems.iterator();
			while (i.hasNext()) {
				fi = (org.apache.commons.fileupload.FileItem) i.next();
				if (!fi.isFormField() && (fi.getFieldName().equals("Archivo") || fi.getFieldName().equals("Archivo_CC") ||
					fi.getFieldName().equals("Archivo_CC_2") || fi.getFieldName().equals("archivo_finiquito"))) {
					if (!fi.getName().equals("")) {
						fieldName = fi.getFieldName(); if (fieldName.equals("archivo_finiquito")) val = false;
						fileName = fi.getName();
						isInMemory = fi.isInMemory();
						sizeInBytes = fi.getSize();

						Consultas c = new Consultas();
						Parametro[] p1 = c.consulta_parametro(8, 0, 0, 0, 0);
						Parametro[] p2 = c.consulta_parametro(9, 0, 0, 0, 0);
						
						if (p1 != null) filePath = p1[0].getValor_Texto() + "siniestro_" + siniestro + SEPARADOR;
						else filePath = context.getInitParameter("file-upload") + "siniestro_" + siniestro + SEPARADOR;
						if (p2 != null) urlPath = p2[0].getValor_Texto() + "siniestro_" + siniestro + "/";
						else urlPath = context.getInitParameter("file-upload-url") + "siniestro_" + siniestro + "/";
	
						file = new java.io.File(filePath);
						if(!file.exists()) {
							System.out.println("*** Creando directorio: " + filePath);
							if (file.mkdirs()) {
								System.out.println("*** Directorio creado.");  
							} else {
								System.out.println("*** El directorio no fue creado.");
							}
						}
	
						if (file.exists()) {
							// Write the file
							fileName = fileName.lastIndexOf(SEPARADOR) > 0 ? fileName.substring(fileName.lastIndexOf(SEPARADOR), fileName.length()) : fileName;
							fileName = util.QuitaCaracteresEspeciales(fileName);			//Quitamos espacios y caracteres especiales.
							file = new java.io.File(filePath + fileName);
							System.out.println(isInMemory + " - " + sizeInBytes);
	
							fi.write(file);
							mensaje += "<p align=\"left\"><b>Archivo actualizado.</b></p>";
						}
					}
				}
			}
			
			//html += "<html>\n";
			//html += "<head>\n";
			//html += "</head>\n";
			//html += "<link rel=\"stylesheet\" href=\"../css/estilo.css\" />\n";
			html += "<body onload=\"plm_carga_archivo()\">\n";
			html += mensaje + "\n";
			html += "</body>\n";
			html += "<script language=\"javascript\">\n";
			html += "<!--\n";
			html += "function plm_carga_archivo() {\n";
			html += "alert(\"Archivo(s) cargado(s).\");\n";
			//html += "parent.plm_control_valor_asigna(\"Archivo_URL\", \"" + urlPath + fileName + "\");\n";
			html += "if (parent.FORMA[\"archivo_finiquito_URL\"]) ";
			html += "parent.plm_control_valor_asigna(\"archivo_finiquito_URL\", \"" + urlPath + fileName + "\");\n";
			html += "if (!parent.plm_control_valida(parent.FORMA[\"fchFiniquito\"], \"texto\", true, \"\", \"\", \"\")) { alert(\"Por favor, capture la fecha de finiquito.\"); parent.FORMA[\"fchFiniquito\"].focus(); }\n";
			//html += "//parent.plm_actualiza_siniestro();\n";
			html += "if (parent.FORMA[\"Archivo\"] || parent.FORMA[\"archivo_finiquito\"]) parent.plm_carga(\"01_registro_siniestro/Registro_Siniestro_7_Documentos.jsp\", \"Registro_Siniestro_7_Documentos\", 2);\n";
			html += "};\n";
			html += "//-->\n";
			html += "</script>\n";
			//html += "</html>";
			//System.out.println(html);
			//out.println(html);

		} catch (Exception ex) {
			System.out.println(ex);
			//html += "<html>\n";
			//html += "<head>\n";
			//html += "</head>\n";
			//html += "<link rel=\"stylesheet\" href=\"../css/estilo.css\" />\n";
			html += "<body>\n";
			html += "</body>\n";
			html += "<script language=\"javascript\">\n";
			html += "<!--\n";
			html += "$(document).ready(function() {\n";
			html += "alert(\"No se carg� el archivo.\");\n";
			html += "});\n";
			html += "//-->\n";
			html += "</script>\n";
			//html += "</html>";
			//out.println(html);
		}
	} else {
		//html += "<html>\n";
		//html += "<head>\n";
		//html += "</head>\n";
		//html += "<link rel=\"stylesheet\" href=\"../css/estilo.css\" />\n";
		html += "<body>\n";
		html += "</body>\n";
		html += "<script language=\"javascript\">\n";
		html += "<!--\n";
		html += "$(document).ready(function() {\n";
		html += "alert(\"No se actualiz� el archivo.\");\n";
		html += "});\n";
		html += "//-->\n";
		html += "</script>\n";
		//html += "</html>";
		//out.println(html);
	} %>
<%=html%>
</html>