/*VARIABLES DE USO GENERAL*/
var NULL_STRING = "";

var DIV_MENU = "#menu";
var DIV_CABEZA = "#cabeza";
var DIV_CONTENIDO = "#contenido";
var DIV_PROCESANDO = "#procesando";
var DIV_ALERTA_VALIDACION = "#alerta_validacion";
var DIV_MENSAJE_VALIDACION = "#Mensaje_Validacion";
var DIV_ACTUALIZAR = "#Actualizar";
var AJAX = null;
var AJAX_STATE = NULL_STRING;

/*AJUSTES PARA EL CONTROL DE FECHA*/
$.datepicker.setDefaults({
	changeMonth: true,
	changeYear: true,
	dateFormat: "dd/mm/yy",
	dayNames: ["Domingo", "Lunes", "Martes", "Mi&eacute;rcoles", "Jueves", "Viernes", "S&aacute;bado"],
	dayNamesShort: ["Dom", "Lun", "Mar", "Mi&eacute;", "Jue", "Vie", "S&aacute;b"],
	dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "S&aacute;"],
	monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", 
	             "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
	monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun",
	             	  "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
	prevText: "anterior",
	nextText: "siguiente",
	minDate: "-1y",
	maxDate: "+0d",
	yearRange: "-100y:+0d"
});

$.timepicker.setDefaults({
	timeFormat: "hh:mm",
    timeText: 'Hora:',
	hourText: 'Seleccionar Horas:',
    minuteText: 'Seleccionar Minutos:',
    currentText: 'Hoy',
	closeText: 'Ok'
});

/*DESHABILITAR BOT�N DERECHO DEL MOUSE*/
document.oncontextmenu = function() { return false; };

/*FUNCION AJAX PARA CARGA DE CONTENIDOS*/
function plm_carga() {
	var args = arguments;
	var pagina = NULL_STRING; var div = NULL_STRING; var centrar = null;
	var muestra = null; var asinc = null;
	if (args.length > 0) pagina = args[0];
	if (args.length > 1) div = args[1];
	if (args.length > 2) muestra = args[2];
	if (args.length > 3) centrar = args[3];
	if (args.length > 4) asinc = args[4];
	pagina = pagina == null ? NULL_STRING : pagina;
	muestra = muestra == null ? 1 : muestra;
	centrar = centrar == null ? false : centrar;
	asinc = asinc == null ? true : asinc;
	div = div == null ? "" : "#" + div.replace(/\#/g, NULL_STRING);
	if ($(div)) {
		if (muestra == 1) {
			//$(div).bind("ajaxStop", function() { $(this).show(); });
			$(div).show();
		} else if (muestra == 2) $(div).show();
		if (pagina != NULL_STRING) {
			AJAX_STATE = NULL_STRING;
			var formData = $("#" + FORMA.name).serializeArray();
			/*var ctype = FORMA.encoding;
			var pdata = true;
			if (ctype.indexOf("multipart/form-data") >= 0 || ctype.indexOf("multipart/mixed") >= 0) {
				formData = new FormData(FORMA[0]); pdata = false;
			}*/
			AJAX = $.ajax({
		    	async: asinc,
				type: "POST",
				url: pagina,
				data: formData,
				cache: false,
				//contentType: ctype + "; charset=UTF-8",
				//processData: pdata,
				success: function (r, s, x) {
					if (r == null) r = NULL_STRING;
					$(div).html(r);
					if (centrar) {
						$(div).css({ position: "absolute", left: 0, top: 0 });
						var alto = $(div).outerHeight() > $(window).height() ? $(window).height() : $(div).outerHeight();
						var ancho = $(div).outerWidth() > $(window).width() ? $(window).width() : $(div).outerWidth();
						var l = ($(window).width() - ancho) / 2; var t = ($(window).height() - alto) / 2; 
						$(div).css({
							position: "fixed", height: alto, width: ancho,
							left: l, top: t, index: 255, overflow: 'auto'
						});
					} AJAX_STATE = "Ok";
				},
				error: function (r, s, x) { AJAX_STATE = r.status + " - " +  x; }
		    });
		}
	}
}

/*FUNCION QUE MUESTRA Y QUITA LA PANTALLA DE PROCESO*/
function plm_procesando() {
	$(window).resize(function () {
		$(DIV_PROCESANDO).css({ position:"absolute", left: 0, top: 0 }).hide();
		$(DIV_PROCESANDO).css({ position:"fixed", width: $(window).width(), height: $(window).height()});
	});
	$(window).resize();
	$(DIV_PROCESANDO).bind("ajaxStart", function () { $(this).show();
	}).bind("ajaxStop", function () { $(this).hide(); });
}

function plm_alerta_validacion() {
	$(DIV_ALERTA_VALIDACION).css({ position:"absolute", left: 0, top: 0,}).hide();
	$(DIV_ALERTA_VALIDACION).css({ position:"fixed", width: $(window).width(), height: $(window).height()});
	var args = arguments; if (args.length && args[0] != null) {
		if (args[0] == 1) $(DIV_ALERTA_VALIDACION).show();
		else plm_foco();
	};
}

/*FUNCION PARA MOSTRAR Y QUITAR FILAS DE UNA CONSULTA*/
function plm_consulta_detalle() {
	var args = arguments;
	var fila = NULL_STRING; var id = NULL_STRING; var pagina = NULL_STRING; var blnVal = false;
	var carga_blanco = true; 
	if (args.length > 0) fila = args[0];
	if (args.length > 1) id = args[1];
	if (args.length > 2) pagina = args[2];
	if (args.length > 3) blnVal = args[3];
	if (args.length > 4) carga_blanco = args[4];
	fila = fila.replace(/\#/gi, NULL_STRING);
	var i = 1; var fila01 = fila + "_" + i; var fila02 = fila + "_datos_" + i;
	var div = fila + "_detalle_" + i;
	while (document.getElementById(fila01) && document.getElementById(fila02)) {
		if (!blnVal) $("#" + fila01).hide(); else $("#" + fila01).show();
		$("#" + fila02).hide(); if (carga_blanco) $("#" + div).html(NULL_STRING); i++;
		fila01 = fila + "_" + i; fila02 = fila + "_datos_" + i; div = fila + "_detalle_" + i;
		$("#paginador_" + fila + "_cierra_detalle").hide();
	}
	fila01 = fila + "_" + id; fila02 = fila + "_datos_" + id; div = fila + "_detalle_" + id; 
	if (document.getElementById(fila01) && document.getElementById(fila02) && !blnVal) {
		$("#" + fila01).show(); $("#" + fila02).show();
		if (carga_blanco) plm_carga(pagina, div, 2);
		$("#paginador_" + fila + "_cierra_detalle").show();
	}
}

//*** AJUSTA LOS VALORES DEL PAGINADOR EN LAS CONSULTAS *** 
function plm_paginador() {
	var args = arguments; var pag = 0; var pagina = "";
	var div = "";
	if (args.length >= 1) pag = args[0];
	if (args.length >= 2) pagina = args[1];
	if (args.length >= 3) div = args[2];
	if (FORMA) FORMA["pag"].value = pag;
	plm_carga(pagina, div, 2);
}

//*** FUNCI�N QUE CAPITALIZA O CONVIERTE EN MAY�SCULAS EL CONTENIDO DE UN CONTROL TEXT O TEXTAREA ***
function plm_capitaliza() {
	var control = null; var args = arguments; if (args.length > 0) control = args[0];
	var up = null; if (args.length > 1) up = args[1]; var texto = NULL_STRING;
	if (control && control.type && (control.type == "textarea" || control.type == "text")) {
		if (up == 1) texto = plm_trim(control.value.toUpperCase());
		else if (up == 2) texto = plm_trim(control.value.toLowerCase());
		else {
			var excepciones = new Array(" de ", " del ", " la ", " las ", " lo ", " los ", " y ", " el ",
										" en ", " su ", " al ", " a ", " e ", " para ", " por ");
			var i = 0; var valor = plm_trim(control.value).toLowerCase();
			while (i < valor.length) {
				if (i == 0) texto += valor.charAt(i).toUpperCase();
				else if (valor.charAt(i - 1) == " ") {
					var j = 0;
					for (j = 0; j < excepciones.length; j++) {
						if (valor.indexOf(excepciones[j]) == i - 1) break;
					} if (j == excepciones.length) texto += valor.charAt(i).toUpperCase();
					else texto += valor.charAt(i);
				} else texto += valor.charAt(i); i++;
			}
		} control.value = texto;
	}
}

function plm_capitaliza_texto() {
	var texto_ini = NULL_STRING;
	var texto = NULL_STRING; var args = arguments; if (args.length > 0) texto_ini = args[0];
	var up = null; if (args.length > 1) up = args[1]; 
	if (texto_ini != NULL_STRING) {
		if (up == 1) texto = plm_trim(texto_ini.toUpperCase());
		else if (up == 2) texto = plm_trim(texto_ini.toLowerCase());
		else {
			var excepciones = new Array(" de ", " del ", " la ", " las ", " lo ", " los ", " y ", " el ",
										" en ", " su ", " al ", " a ", " e ", " para ", " por ");
			var i = 0; var valor = plm_trim(texto_ini).toLowerCase();
			while (i < valor.length) {
				if (i == 0) texto += valor.charAt(i).toUpperCase();
				else if (valor.charAt(i - 1) == " ") {
					var j = 0;
					for (j = 0; j < excepciones.length; j++) {
						if (valor.indexOf(excepciones[j]) == i - 1) break;
					} if (j == excepciones.length) texto += valor.charAt(i).toUpperCase();
					else texto += valor.charAt(i);
				} else texto += valor.charAt(i); i++;
			}
		}
	} return texto;
}

//*** FUNCION QUE QUITA ESPACIOS DOBLES Y ESPACIOS ANTES Y DESPUES DEL TEXTO CAPTURADO EN EL CONTROL INDICADO ***
function plm_trim() {
	var texto = NULL_STRING; var args = arguments; if (args.length > 0) texto = args[0];
	if (texto != NULL_STRING) {
		var doble = "  "; var espacio = " "; var rExp = new RegExp(doble);
		while(texto.indexOf(doble) >= 0) texto = texto.replace(rExp, espacio);
		if (texto.indexOf(espacio) == 0) texto = texto.substring(1, texto.length);
		if (texto.lastIndexOf(espacio) == texto.length - 1) texto = texto.substring(0, texto.lastIndexOf(espacio));
	} if (texto == null) texto = NULL_STRING;
	return texto;
}

//*** FUNCI�N PARA REEMPLAZAR ACENTOS ***
function plm_reemplaza_acentos() {
	var texto = NULL_STRING; var args = arguments; if (args.length > 0) texto = args[0];
	var acentos = "�������������������� ";
	var reemplazo = "aeiouAEIOUaeiouAEIOU_";
	for (var i = 0; i < acentos.length - 1; i++) {
		var acento = acentos.substring(i, i + 1); var rExp = new RegExp(acento, "g");
		texto = texto.replace(rExp, reemplazo);
	} return texto;
}

//*** OBTIENE LA SUMA DE LAS CANTIDADES DE VARIOS CONTROLES TEXT Y LA ASIGNA AL CONTROL TEXT INDICADO ***
function plm_suma() {
	var args = arguments;
	if (FORMA && args.length) {
		var resultado = args[0];
		if (FORMA[resultado] && FORMA[resultado].type == "text") {
			FORMA[resultado].value = 0;
			if (args.length > 1) {
				for (var i = 1; i < args.length; i++) {
					if (FORMA[args[i]] && FORMA[args[i]].type == "text")
						FORMA[resultado].value = new Number(FORMA[resultado].value) + new Number(FORMA[args[i]].value);
				}; FORMA[resultado].value = new Number(FORMA[resultado].value).toFixed(2);
			};
		};
	}; 
}

function plm_foco () {
	if (FOCO && FOCO.form && !FOCO.disabled && FOCO.style.display == "" && FOCO.type != "hidden") {
		FOCO.focus(); FOCO = null;
	};
}

function plm_decimales() {
	var dato = arguments[0]; var digitos = arguments[1];
	var sNumero = ""; //var ceros = "";
	if (dato && dato.type && dato.type.indexOf("text") >= 0) numero = dato.value;
	else numero = dato;
	//Elimina las comas.
	var rexp = new RegExp(",", "g");
	numero = numero.toString().replace(rexp, "");
	//Valor default 0.
	if (numero == null || numero == NULL_STRING || isNaN(numero)) numero = 0;
	//D�gitos default 2.
	if (digitos == null || digitos == NULL_STRING || isNaN(digitos)) digitos = 2;
	if (numero != null && !isNaN(numero) && !isNaN(digitos) && digitos > 0) {
		sNumero = (new Number(numero)).toFixed(digitos).toString();
		/*for (var i = 1; i <= digitos; i++) ceros += "0";
		var punto = numero.toString().indexOf(".");
		if (punto < 0) sNumero = numero + "." + ceros;
		else {
			punto = punto + 1; sNumero = numero + ceros;
			sNumero = sNumero.substring(0, punto + digitos);
		}*/
	} if (dato && dato.type && dato.type.indexOf("text") >= 0) dato.value = sNumero;
	return sNumero;
}
