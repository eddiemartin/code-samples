/***********/
/*personApp*/
/***********/

//module
angular.module ("personApp", ["language", "person", "address", "contact", "family", "warning", "submit"])
//directive
.directive ("personApp", function () {
	return {
		restrict: "E",
		scope: {},
		templateUrl: baseUrl + "../common/templates/apps/personApp.template.html",
		controller:
			function ($scope) {
				$scope.language = "";
				$scope.$on("language", function(evt, lang) {
	    			$scope.language = lang;
	    		});
				if ($scope.$parent.language) {
	    			$scope.language = $scope.$parent.language;
	    		}
			}
	}
});

/**************/
/*personApp v2*/
/**************/

//module
angular.module ("personAppV2", ["language", "person", "address", "contactV2", "familyV2", "warning", "submit"])
//directive
.directive ("personAppV2", function () {
	return {
		restrict: "E",
		scope: {},
		templateUrl: baseUrl + "../common/templates/apps/personApp.v2.template.html",
		controller:
			function ($scope) {
				$scope.language = "";
				$scope.$on("language", function(evt, lang) {
	    			$scope.language = lang;
	    		});
				if ($scope.$parent.language) {
	    			$scope.language = $scope.$parent.language;
	    		}
			}
	}
});

/**********/
/*mainMenu*/
/**********/

//module
angular.module ("mainMenu", ["language", "personApp", "personAppV2"])
//directive
.directive ("mainMenu", function () {
	return {
		restrict: "E",
		templateUrl: baseUrl + "../common/templates/modules/main.menu.template.html"
	}
})
//controller
.controller ("MainMenuController", function ($scope, $http) {
	$scope.json = baseUrl + "../common/json/modules/main.menu.json";
	$scope.$on ("language", function (evt, lang) {
		$scope.language = lang;
		getJsonData($http, $scope, $scope.json, $scope.language);
		GenericController($scope, $http);
	});
	getJsonData($http, $scope, $scope.json, $scope.language);
	GenericController($scope, $http);
});
