/**********/
/*language*/
/**********/

//module
angular.module ("language", [])
//directive
.directive ("language", function () {
	return {
		restrict: "E",
		templateUrl: baseUrl + "../common/templates/modules/language.template.html"
	}
})
//controller
.controller ("LanguageController", function ($scope, $http) {
	$scope.language = (window.navigator.language || navigator.browserLanguage).substring(0, 2);
	$scope.setLanguage = function () {
		if ($scope.language) {
			$scope.$parent.$broadcast("language", $scope.language);
			$scope.$parent.language = $scope.language;
		}
		$scope.json = baseUrl + "../common/json/modules/language.json";
		getJsonData($http, $scope, $scope.json, $scope.language);
		GenericController($scope, $http);
	};
	$scope.setLanguage();
});

/********/
/*person*/
/********/

//module
var person = angular.module ("person", ["language", "countries", "dateControl", "submit"])
//directive
.directive ("person", function () {
    return {
    	restrict: "E",
    	scope: {
    		ctrlName: "@fieldName", showIdent: "<ident", showBirthday: "<birthday",
    		showPhysics: "<physics", showRelation: "<relation", showBirthPlace: "<birthplace",
    		isRequired: "<required", showCaption: "<caption"
    	},
    	templateUrl: baseUrl + "../common/templates/modules/person.template.html",
    	controller:
	    	function ($scope, $http) {
	    		$scope.json = baseUrl + "../common/json/modules/person.json";
	    		$scope.relationOptions = { caption: null, data: null }
	    		$scope.json01 = baseUrl + "../common/json/modules/relationship.json";
	    		$scope.$on("language", function(evt, lang) {
	    			$scope.language = lang;
	    			getJsonData($http, $scope, $scope.json, $scope.language);
	    			getJsonData($http, $scope.relationOptions, $scope.json01, $scope.language);
	    		});
	    		if ($scope.$parent.language) {
	    			$scope.language = $scope.$parent.language;
	    		}
	    		getJsonData($http, $scope, $scope.json, $scope.language);
	    		getJsonData($http, $scope.relationOptions, $scope.json01, $scope.language);
	    		GenericController($scope, $http);
	    	}
    }
});

/*********/
/*address*/
/*********/

//module
angular.module ("address", ["language", "countries", "submit"])
//directive
.directive ("address", function () {
    return {
    	restrict: "E",
    	scope: {},
    	templateUrl: baseUrl + "../common/templates/modules/address.template.html",
    	controller:
    		function ($scope, $http) {
	    		$scope.json = baseUrl + "../common/json/modules/address.json";
	    		$scope.$on ("language", function (evt, lang) {
	    			$scope.language = lang;
	    			getJsonData($http, $scope, $scope.json, $scope.language);
	    		});
	    		if ($scope.$parent.language)
	    			$scope.language = $scope.$parent.language;
	    		getJsonData($http, $scope, $scope.json, $scope.language);
	    		GenericController($scope, $http);
    		}
    }
});

/*********/
/*contact*/
/*********/

//module
angular.module ("contact", ["language", "submit"])
//directive
.directive ("contact", function () {
	return {
		restrict: "E",
		scope: {},
		templateUrl: baseUrl + "../common/templates/modules/contact.template.html",
		controller:
			function ($scope, $http) {
				$scope.rows = []; $scope.rows[0] = { caption: null, data: null }
				$scope.json = baseUrl + "../common/json/modules/contact.json";
				$scope.$on ("language", function (evt, lang) {
					$scope.language = lang;
					getJsonData($http, $scope.rows, $scope.json, $scope.language);
				});
				if ($scope.$parent.language)
					$scope.language = $scope.$parent.language;
				getJsonData($http, $scope.rows, $scope.json, $scope.language);
				GenericController($scope, $http);
			}
	}
});

/************/
/*contact v2*/
/************/

//module
angular.module ("contactV2", ["language", "submit"])
//directive
.directive ("contactV2", function () {
	return {
		restrict: "E",
		scope: {},
		templateUrl: baseUrl + "../common/templates/modules/contact.v2.template.html",
		controller:
			function ($scope, $http) {
				$scope.editRows = []; $scope.rowToEdit = null;
				$scope.json = baseUrl + "../common/json/modules/contact.json";
				$scope.$on ("language", function (evt, lang) {
					$scope.language = lang;
					getJsonData($http, $scope, $scope.json, $scope.language);
				});
				if ($scope.$parent.language)
					$scope.language = $scope.$parent.language;
				getJsonData($http, $scope, $scope.json, $scope.language);
				GenericController($scope, $http);
			}
    }
});

/********/
/*family*/
/********/

//module
angular.module ("family", ["language", "submit"])
//directive
.directive ("family", function () {
	return {
		restrict: "E",
		scope: {
			showBirthday: "<birthday"
		},
		templateUrl: baseUrl + "../common/templates/modules/family.template.html",
		controller: 
			function ($scope, $http, $attrs) {
				$scope.rows = []; $scope.rows[0] = { caption: null, data: null }
				$scope.json = baseUrl + "../common/json/modules/family.json";
				$scope.$on ("language", function (evt, lang) {
					$scope.language = lang;
					getJsonData($http, $scope.rows, $scope.json, $scope.language);
				});
				if ($scope.$parent.language)
					$scope.language = $scope.$parent.language;
				getJsonData($http, $scope.rows, $scope.json, $scope.language);
				GenericController($scope, $http);
			}
	}
});

/***********/
/*family v2*/
/***********/

//module
angular.module ("familyV2", ["language", "submit"])
//directive
.directive ("familyV2", function () {
	return {
		restrict: "E",
		scope: {
			showBirthday: "<birthday"
		},
		templateUrl: baseUrl + "../common/templates/modules/family.v2.template.html",
		controller:
			function ($scope, $http, $attrs) {
				$scope.editRows = []; $scope.rowToEdit = null;
				$scope.json = baseUrl + "../common/json/modules/family.json";
				$scope.relationOptions = { caption: null, data: null }
				$scope.json01 = baseUrl + "../common/json/modules/relationship.json";
				$scope.$on ("language", function (evt, lang) {
					$scope.language = lang;
					getJsonData($http, $scope, $scope.json, $scope.language);
					getJsonData($http, $scope.relationOptions, $scope.json01, $scope.language);
				});
				if ($scope.$parent.language)
					$scope.language = $scope.$parent.language;
				getJsonData($http, $scope, $scope.json, $scope.language);
				getJsonData($http, $scope.relationOptions, $scope.json01, $scope.language);
				GenericController($scope, $http);
			}
    }
});

/********/
/*submit*/
/********/

//module
angular.module ("submit", ["language"])
//directive
.directive ("submit", function () {
	return {
		restrict: "E",
		scope: {
			ctrlForm: "@form", isDisabled: "<disabled"
		},
		templateUrl: baseUrl + "../common/templates/modules/submit.template.html",
		controller:
			function ($scope, $http) {
				$scope.json = baseUrl + "../common/json/modules/submit.json";
				$scope.$on ("language", function (evt, lang) {
					$scope.language = lang;
					getJsonData($http, $scope, $scope.json, $scope.language);
				});
				if ($scope.$parent.language)
					$scope.language = $scope.$parent.language;
				getJsonData($http, $scope, $scope.json, $scope.language);
				GenericController($scope, $http);
			}
	}
});

/*********/
/*warning*/
/*********/

//module
angular.module ("warning", ["language"])
//directive
.directive ("warning", function () {
	return {
		restrict: "E",
		scope: {},
		templateUrl: baseUrl + "../common/templates/modules/warning.template.html",
		controller:
			function ($scope, $http) {
				$scope.json = baseUrl + "../common/json/modules/warning.json";
				$scope.$on ("language", function (evt, lang) {
					$scope.language = lang;
					getJsonData($http, $scope, $scope.json, $scope.language);
				});
				if ($scope.$parent.language)
					$scope.language = $scope.$parent.language;
				getJsonData($http, $scope, $scope.json, $scope.language);
				GenericController($scope, $http);
			}
    }
});

/***********/
/*countries*/
/***********/

//module
angular.module ("countries", ["language", "submit"])
//directive
.directive ("countries", function () {
	return {
		restrict: "E",
		scope: {
			ctrlName: "@name", isDisabled: "<disabled", isRequired: "<required",
			isNationality: "<nationality"
		},
		templateUrl: baseUrl + "../common/templates/modules/countries.template.html",
		controller:
			function ($scope, $http) {
				$scope.json = baseUrl + "../common/json/modules/countries.json"
				$scope.$on("language", function(evt, lang) {
					$scope.language = lang;
					getJsonData($http, $scope, $scope.json, $scope.language);
				});
				if ($scope.$parent.language)
					$scope.language = $scope.$parent.language;
				getJsonData($http, $scope, $scope.json, $scope.language);
				GenericController($scope, $http);
			}
    }
});

/******/
/*date*/
/******/

//module
angular.module ("dateControl", ["language", "submit"])
//directive
.directive("dateControl", function () {
	return {
		restrict: 'E',
		scope: {
			ctrlName: "@fieldName", ctrlPattern: "@pattern", ctrlPlaceHolder: "@placeholder",
			isRequired: "<required", isDisabled: "<disabled",
			ctrlWarning: "@warning", ctrlLabel: "@label", showAge: "<showAge",
			setMin: "@min", setMax: "@max"
		},
		template: (isInputDate() ?
				"<input type=\"date\" ng-model=\"datePick\" name=\"{{ctrlName}}\" " +
				"placeholder=\"{{ctrlPlaceHolder}}\" min=\"{{getFormatDate(setMin)}}\" " +
				"max=\"{{getFormatDate(setMax)}}\" ng-required=\"isRequired\" " +
				"ng-disabled=\"isDisabled\" />" :
				"<input type=\"text\" ng-model=\"datePick\" name=\"{{ctrlName}}\" " +
				"ng-required=\"isRequired\" pattern=\"{{ctrlPattern}}\" " +
				"ng-disabled=\"isDisabled\" placeholder=\"{{ctrlPlaceHolder}}\" " +
				"datepicker/>") +
				"<span class=\"validate\"></span>" +
				"<div class=\"warning ng-hide\" ng-show=\"isEmptyField('{{ctrlName}}')&&!isDisabled\">" +
				"{{ctrlWarning}}</div>" +
				"<div class=\"warning ng-hide\" " +
				"ng-show=\"!isEmptyField('{{ctrlName}}')&&!isDisabled&&showAge&&getAge(datePick)>0\">" +
				"{{getAge(datePick)}} {{ctrlLabel}}</div>",
		//templateUrl: baseUrl + (isInputDate() ? "../common/templates/modules/date.template.html" :
		//	"../common/templates/modules/datepicker.template.html"),
		controller:
			function ($scope, $http) {
				if ($scope.$parent.language)
					$scope.language = $scope.$parent.language;
				GenericController($scope, $http);
			}
    };
})
.directive("datepicker", function () {
	return {
		restrict: "A",
		require: "ngModel",
		link:
			function (scope, element, attrs, ctrl) {
				$(element).datepicker ({
					dateFormat: scope.language == "es" ? "dd/mm/yy" :
						scope.language == "en" ? 'mm/dd/yy' : "yy/mm/dd",
					onSelect: function (datePick) {
						ctrl.$setViewValue(datePick);
						ctrl.$render();
						scope.$apply();
					},
					changeMonth: true,
					changeYear: true
				});
				scope.$watch("setMin", (function (newValue, oldValue) {
					//var dt = scope.getFormatDate(newValue, scope.language);
					$(element).datepicker("option", "minDate", newValue + "y");
				}), true);
				scope.$watch("setMax", (function (newValue, oldValue) {
					//var dt = scope.getFormatDate(newValue, scope.language);
					$(element).datepicker("option", "maxDate", newValue + "y");
				}), true);
				scope.$on("language", function(evt, lang) {
					scope.language = lang;
					$(element).datepicker ("option", "dateFormat",
						scope.language == "es" ? "dd/mm/yy" :
						scope.language == "en" ? 'mm/dd/yy' : "yy/mm/dd");
				});
			}
    };
});