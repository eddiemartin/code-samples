/*********************/
/* Angular functions */
/*********************/
function getForm() {
	var scope = arguments[0];
	if (scope) {
		var formName = arguments[1];
		if (!formName || formName == null || formName == "") {
			formName = "form";
		} if (scope.$parent) {
			if (scope.$parent[formName]) {
				return scope.$parent[formName];
			} else return getForm(scope.$parent, formName);
		} else return null;
	} else return null;
}

function getField() {
	var scope = arguments[0];
	if (scope) {
		var field = arguments[1];
		var formName = arguments[2];
		var form = getForm(scope, formName);
		if (form && form[field]) return form[field];
	} else return null;
}

function setAngularFieldValue() {
	var scope = arguments[0];
	if (scope != null) {
		var fieldName = arguments[1];
		var value = arguments[2];
		var x = eval("scope." + fieldName);
		if (x == null)
			setAngularFieldValue(scope.$$childHead, fieldName, value);
		else {
			var dt = null;
			if (isString(value)) dt = setDate(value);
			value = isDate(dt) ? "setDate('" + value + "')" : "'" + value + "'";
			x = "scope." + fieldName +"=" + value; 
			eval(x);
		}
	}
}

//gets data from json files
function getJsonData () {
	var http = arguments[0];
	var obj = arguments[1];
	var json = arguments[2];
	var language = arguments[3];
	if (language == null || language == "")
		language = (window.navigator.language || navigator.browserLanguage).substring(0, 2);
	if (http && json && json != "") {
		http.get(json).then (function (response) {
			var data = response.data;
			if (obj && obj.length) {
				for (row in obj) {
					for (var i in data) {
						if (language == data[i].language) {
							obj[row].caption = data[i].caption;
							obj[row].data = data[i].data;
							break;
						}
					}
				}
			} else {
				for (var i in data) {
					if (language == data[i].language) {
						obj.caption = data[i].caption;
						obj.data = data[i].data;
						break;
					}
				}
			}
		})
	};
}

//generic controller function
function GenericController () {
	var scope = arguments[0];
	var http = arguments[1];
	//validates data fields in controller
	scope.isNotValidField = function () {
		var field = arguments[0];
		var formName = arguments[1];
		var obj = getField(scope, field, formName);
		if (obj) {
			var o = obj.$error;
			return (o.required || o.pattern || o.min || o.max || o.date);
		} else return false;
	};
	//
	scope.isEmptyField = function () {
		if (!scope.isNotValidField(arguments[0])) {
			var val = getFieldValue(arguments[0]);
			if (val == null || val == "") return true;
			else return false;
		} else return true;
	};
	scope.getForm = function () {
		var formName = arguments[0];
		return getForm(scope, formName);
	};
	//validates parent data form
	scope.isNotValidForm = function() {
		var formName = arguments[0];
		var form = getForm(scope, formName);
		if (form && form.$invalid) {
			return form.$invalid;
		} else return false;
	};
	//adds a row in controller
	scope.add = function () {
		var args = arguments; var row = null;
		if (args.length && scope.editRows) {
			row = scope.rowToEdit;
			if (row == null) {
				row = scope.editRows.length;
				scope.editRows[row] = scope.editRows[row - 1];
			}
			if (args.length) {
				var x = "scope.editRows[" + row + "]={"; var y = "";
				for (var i = 0; i < args.length; i++) {
					var prop = args[i];
					if (prop.indexOf(".") >= 0)
						prop = prop.substring(prop.lastIndexOf(".") + 1, prop.length);
					var val = getFieldValue(args[i]);
					x += prop + ":'" + (val == null ? "" : val) + "',";
					y += "setAngularFieldValue(scope,'" + prop + "','');"
				} x = x.substring(0, x.length - 1) + "};";
				eval(x + y); scope.rowToEdit = null;
			}
		}
		else if (scope.rows) {
			row = scope.rows.length;
			scope.rows[row] = scope.rows[row - 1];
		}
	};
	//drops last row in controller
	scope.del = function () {
		if (scope.rows) {
			var row = arguments[0];
			if (scope.rows.length > 1) {
				if (isNaN(row)) row = scope.rows.length - 1;
				scope.rows.splice(row, 1);
			}
		} else if (scope.editRows) {
			var row = arguments[0];
			if (scope.editRows.length > 1) {
				if (isNaN(row)) row = scope.editRows.length - 1;
				scope.editRows.splice(row, 1);
				scope.rowToEdit = null;
			}
		}
	};
	//edits row data
	scope.edit = function () {
		var row = arguments[0]; var group = arguments[1];
		if (!isNaN(row) && scope.editRows) {
			scope.rowToEdit = row;
			var r = scope.editRows[row]; var x = "";
			if (!group) group = ""; else group += ".";
			for (item in r) {
				var dt = null; var prop = "";
				x += setAngularFieldValue(scope, item, r[item]);
			}
		}
	};
	//sets defaults values when select control value is ""
	scope.setDefaultOption = function () {
		var obj = arguments[0]; var i = arguments[1];
		if (obj && obj.length) {
			if (i && !isNaN(i)) return obj[i]; else return obj[0];
		} else return null;
	};
	//sets options to select control by value 
	scope.getOptions = function () {
		var obj = arguments[0]; var val = arguments[1];
		var name = arguments[2]; var options = arguments[3];
		if (val == null) val = "";
		if (obj && obj.length && name && options) {
			for (var i = 0; i < obj.length; i++) {
				if (obj[i][name] == val) {
					return obj[i][options]; break;
				}
			}
		} else return null;
	};
	//gets an option
	scope.getOption = function () {
		var options = arguments[0];
		var value = arguments[1]; var option = null;
		if (options && options.length) {
			for (var i = 0; i < options.length; i++) {
				if (options[i].value == value) {
					option = options[i]; break;
				}
			}
		} return option;
	};
	//gets age from a date
	scope.getAge = function() {
		var dt = arguments[0];
		if (!isInputDate())
			dt = setDate(dt, scope.language);
		//console.log(dt);
		return getAge(dt);
	};
	//gets date (today by default)
	scope.getFormatDate = function () {
		var year = arguments[0];
		var month = arguments[1];
		var day = arguments[2];
		var language = arguments[3];
		var dt = getFormatDate(year, month, day, language);
		return dt;
	};
	scope.setDate = function () {
		return setDate(arguments[0]);
	}
	scope.addRemoveClass = function () {
		var name = arguments[0]; var attr = arguments[1];
		var val = arguments[2];
		if (val != null && val != "")
			document.getElementsByTagName("select")[name].setAttribute("class", "");
		else
			document.getElementsByTagName("select")[name].setAttribute("class", attr);
	}
	scope.getUrl = function (event) {
		var href = arguments[0];
		scope.$parent.content = href;
		activate(event, "menu", "menu", "menu_responsive");
	}
};