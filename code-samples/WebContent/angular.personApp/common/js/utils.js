/*************************/
/* Non Angular functions */
/*************************/

function setDate() {
	var y = arguments[0];
	var m = arguments[1];
	var d = arguments[2];
	var dtNew = null;
	if (!isDate(y) && isString(y)) {
		var dt = y.replace(/-/g, "").replace(/\//g, "");
		if (isNumber(dt) && dt.length == 8) {
			if (m != null && m.indexOf("es") == 0) {
				y = new Number(dt.substring(4, 8));
				m = new Number(dt.substring(2, 4));
				d = new Number(dt.substring(0, 2));
			} else if (m != null && m.indexOf("en") == 0) {
				y = new Number(dt.substring(4, 8));
				m = new Number(dt.substring(0, 2));
				d = new Number(dt.substring(2, 4));
			} else {
				y = new Number(dt.substring(0, 4));
				m = new Number(dt.substring(4, 6));
				d = new Number(dt.substring(6, 8));
			} dtNew = new Date(y, m - 1, d);
		} else {
			dt = new Date(y);
			if (isDate(dt)) dtNew = dt1;
		}
	} else if (isNumber(y)) {
		dt = new Date();
		y = dt.getFullYear() + new Number(y);
		m = isNumber(m) ? new Number(m) - 1 : dt.getMonth();
		d = isNumber(d) ? new Number(d) : dt.getDate();
		dtNew = new Date(y, m, d);
	} return dtNew;
}

function getFormatDate() {
	var newDate = ""; var dt = null;
	var y = arguments[0]; var m = arguments[1];
	var d = arguments[2]; var l = arguments[3];
	if (isNumber(y) && isNumber(m) && isNumber(d))
		dt = setDate(y, m - 1, d);
	else {
		l = m; dt = setDate(y, l);
	}
	if (isDate(dt)) {
		y = dt.getFullYear(); m = dt.getMonth() + 1; d = dt.getDate();
		if (l && "es".indexOf(l) >= 0) {
			newDate = (d < 10 ? "0" : "") + d + "/" + (m < 10 ? "0" : "") + m + "/" + y;
		} else if (l && "en".indexOf(l) >= 0) {
			newDate = (m < 10 ? "0" : "") + m + "/" + (d < 10 ? "0" : "") + d + "/" + y;
		} else newDate = y + "-" + (m < 10 ? "0" : "") + m + "-" + (d < 10 ? "0" : "") + d;
	}
	return newDate;
}

function isDate() {
	var x = arguments[0];
	if (x != null && x instanceof Date &&
		!isNaN(x.getTime())) return true;
	else return false;
}

function isString() {
	var x = arguments[0];
	if (x != null && typeof(x) == "string" &&
		isNaN(x)) return true;
	else return false;
}

function isNumber() {
	var x = arguments[0];
	if (x != null && !isNaN(x) && !isDate(x)) return true;
	else return false;	
}

function getAge() {
	var dt1 = arguments[0]; var dt2 = arguments[1];
	if (!dt1 || !(dt1 instanceof Date)) dt1 = setDate(dt1);
	if (!dt2 || !(dt2 instanceof Date)) dt2 = setDate(dt2);
	if (!dt1 || !(dt1 instanceof Date)) dt1 = new Date();
	if (!dt2 || !(dt2 instanceof Date)) dt2 = new Date();
	if (dt1 < dt2) {
		var year1 = dt1.getFullYear(); var month1 = dt1.getMonth() + 1; var day1 = dt1.getDate();
		var year2 = dt2.getFullYear(); var month2 = dt2.getMonth() + 1; var day2 = dt2.getDate();
	} else {
		var year1 = dt2.getFullYear(); var month2 = dt2.getMonth() + 1; var day2 = dt2.getDate();
		var year2 = dt1.getFullYear(); var month1 = dt1.getMonth() + 1; var day1 = dt1.getDate();
	}
	var age = year2 - year1 + (month2 < month1 ? -1 : 0) + (month2 == month1 && day2 < day1 ? -1 : 0);
	return age;
}

function trim() {
	var input = arguments[0];
	if (input && input.length > 0) {
		var rexp = /\s\s/g;
		while(input.indexOf("  ") >= 0) input = input.replace(rexp, " ");
		while(input.charAt(0) == " ") input = input.substring(1, input.length);
		while(input.charAt(input.length - 1) == " ") input = input.substring(0, input.length - 1);
	} return input;
}

function capitalize() {
	var input = arguments[0]; var txt = "";
	if (input.value) txt = input.value; else txt = input;
	if (txt.length > 0) {
		var x = arguments[1]; txt = trim(txt);
		var words = txt.split(" "); txt = "";
		for (var i = 0; i < words.length; i++) {
			if (x && !isNaN(x) && i >= x)
				txt += words[i].charAt(0) +
					words[i].substring(1, words[i].length).toLowerCase() + " ";
			else
				txt += words[i].charAt(0).toUpperCase() +
						words[i].substring(1, words[i].length).toLowerCase() + " ";
		} if (input.value) input.value = trim(txt);
	} return trim(txt);
}

function uppercase() {
	var input = arguments[0];
	if (input.value) txt = input.value; else txt = input;
	if (txt.length > 0) {
		txt = txt.toUpperCase();
		if (input.value) input.value = trim(txt);
	} return trim(txt);
}

function lowercase() {
	var input = arguments[0];
	if (input.value) txt = input.value; else txt = input;
	if (txt.length > 0) {
		txt = txt.toLowerCase();
		if (input.value) input.value = trim(txt);
	} return trim(txt);
}

function activate() {
	var evt = arguments[0]; var name = arguments[1];
	var css1 = arguments[2]; var css2 = arguments[3];
	var css3 = arguments[4];
	var x = document.getElementById(name);
	var w = window.innerWidth;
	if (x.className != css2 && evt.type == "click") {
        	x.className = css2;
	} else {
		 if (css3 != null) {
	       x.className = css3;
		 } else x.className = css1;
	}
}

function getDocField() {
	var name = arguments[0];
	var form = arguments[1];
	if (!form || form == "") form = "form";
	if (document.forms[form] && document.forms[form][name]) {
		return document.forms[form][name];
	} else return null;
}

function getFieldValue() {
	var fieldName = arguments[0];
	var formName = arguments[1];
	if (formName == null || formName == "") formName = "form";
	var form = document.forms[formName];
	if (form) {
		for (var i = 0; i < form.length; i++) {
			if (form[i].name == fieldName) {
				var fld = form[i];
				//select
				if (fld.type == "select-one") {
					return fld.options[fld.selectedIndex].value;
				//checkbox & radio
				} else if (fld.type == "checkbox" || fld.type == "radio") {
					if (fld.checked) return fld.value;
				//input, textarea, etc.
				} else return fld.value;
			}
		}
	} else return null;
}

function setFieldValue() {
	var fieldName = arguments[0];
	var val = arguments[1];
	var formName = arguments[2];
	if (formName == null || formName == "") formName = "form";
	var form = document.forms[formName];
	if (form) {
		for (var i = 0; i < form.length; i++) {
			if (form[i].name == fieldName) {
				var fld = form[i];
				//select
				if (fld.type == "select-one") {
					for (var j = 0; j < fld.options.length; j++) {
						if (fld.options[j].value == val) {
							fld.selectedIndex = j; break;
						}
					}
					if (j == fld.options.length) {
						fld.selectedIndex = 0;
					} else break;
				//checkbox & radio
				} else if (fld.type == "checkbox" || fld.type == "radio") {
					if (fld.value == val) {
						fld.checked = true; break;
					}
					else fld.checked = false;
				//input, textarea, etc.
				} else {
					fld.value = val; break;
				}
			}
		}
	}
}

function isInputDate () {
    var isDate = false;
    var test = document.createElement("input");
    test.setAttribute("type", "date");
    if (test.type == "date") {
        test.setAttribute('value', 'InvalidDate');
        if (test.value != 'InvalidDate') {
        	isDate = true;
        };
    } return isDate;
}