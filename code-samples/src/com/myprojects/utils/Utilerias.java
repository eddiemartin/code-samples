package com.myprojects.utils;

import java.text.ParseException;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

@SuppressWarnings("unused")
public class Utilerias {
	public String parametros = "";
	
	public Utilerias() {}

	private String QuitaNulos(String texto) {
		if (texto == null) texto = "";
		else texto = texto.trim();
		return texto;
	}
	
	public String QuitaEspaciosDobles(String texto) {
		String log = "\n*** QUITAR ESPACIOS DOBLES ***\n*** TEXTO DE ENTRADA: " + texto + "\n";
		texto = QuitaNulos(texto);
		while (texto.indexOf("  ") >= 0) {
			texto = texto.replace("  ", " ");
		} log += "*** TEXTO DE SALIDA: " + texto + "\n";
		//System.out.println(log);
		return texto;
	}

	public String QuitaEspaciosDobles(String texto, String especiales, String reemplazo) {
		String log = "\n*** QUITAR ESPACIOS DOBLES ***\n*** TEXTO DE ENTRADA: " + texto + "\n";
		texto = QuitaNulos(texto);
		while (texto.indexOf("  ") >= 0) {
			texto = texto.replace("  ", " ");
		} texto = texto.replace(especiales, reemplazo);
		log += "*** TEXTO DE SALIDA: " + texto + "\n";
		//System.out.println(log);
		return texto;
	}

	public String QuitaCaracteresEspeciales(String texto) {
		String texto_nuevo = "";
		if (!QuitaEspaciosDobles(texto).equals("")) {
			String validos = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._-";
			texto = texto.replace(" ", "_");
			texto = texto.replace("�", "A"); texto = texto.replace("�", "a");  
			texto = texto.replace("�", "E"); texto = texto.replace("�", "e");
			texto = texto.replace("�", "I"); texto = texto.replace("�", "i");
			texto = texto.replace("�", "O"); texto = texto.replace("�", "o");
			texto = texto.replace("�", "U"); texto = texto.replace("�", "u");
	
			texto = texto.replace("�", "A"); texto = texto.replace("�", "a");  
			texto = texto.replace("�", "E"); texto = texto.replace("�", "e");
			texto = texto.replace("�", "I"); texto = texto.replace("�", "i");
			texto = texto.replace("�", "O"); texto = texto.replace("�", "o");
			texto = texto.replace("�", "U"); texto = texto.replace("�", "u");
	
			texto = texto.replace("�", "A"); texto = texto.replace("�", "a");  
			texto = texto.replace("�", "E"); texto = texto.replace("�", "e");
			texto = texto.replace("�", "I"); texto = texto.replace("�", "i");
			texto = texto.replace("�", "O"); texto = texto.replace("�", "o");
			texto = texto.replace("�", "U"); texto = texto.replace("�", "u");
	
			for (int i = 0; i < texto.length(); i++) {
				String letra = "" + texto.charAt(i);
				if (validos.indexOf(letra.toUpperCase()) >= 0) texto_nuevo += letra;
			}
		}
		return texto_nuevo;
	}
	
	public int ObtieneInt(String texto) {
		String log = "\n*** OBTENER ENTERO ***\n*** TEXTO DE ENTRADA: " + texto + "\n";
		int numero = 0;
		try {
			texto = texto.replace(",", ""); texto = texto.replace("$", "");  
			numero = Integer.parseInt(QuitaEspaciosDobles(texto));
		} catch (Exception e) {
			log += "\n!!! NO ES INTEGER: " + texto + ". " + e.getMessage() + "\n";
			numero = 0;
		} log += "*** ENTERO DE SALIDA: " + numero + "\n";
		//System.out.println(log);
		return numero;
	}

	public long ObtieneLong(String texto) {
		String log = "\n*** OBTENER LONG ***\n*** TEXTO DE ENTRADA: " + texto + "\n";
		long numero = 0;
		try {
			texto = texto.replace(",", ""); texto = texto.replace("$", "");
			numero = Long.parseLong(QuitaEspaciosDobles(texto));
		} catch (Exception e) {
			log += "\n!!! NO ES LONG: " + texto + ". " + e.getMessage() + "\n";
			numero = 0;
		} log += "*** LONG DE SALIDA: " + numero + "\n";
		//System.out.println(log);
		return numero;
	}
	
	public double ObtieneDouble(String texto) {
		String log = "\n*** OBTENER DOUBLE ***\n*** TEXTO DE ENTRADA: " + texto + "\n";
		double numero = 0;
		try {
			texto = texto.replace(",", ""); texto = texto.replace("$", "");
			numero = Double.parseDouble(QuitaEspaciosDobles(texto));
		} catch (Exception e) {
			log += "\n!!! NO ES DOUBLE: " + texto + ". " + e.getMessage() + "\n";
			numero = 0;
		} log += "*** DOUBLE DE SALIDA: " + numero + "\n";
		//System.out.println(log);
		return numero;
	}
	
	//La variable texto debe tener formato de fecha SQL (aaaa-mm-dd HH:mm:ss), dd/mm/aaaa o mm/dd/aaaa.
	public java.util.Date ObtieneDate(String texto) {
		String log = "\n*** OBTENER FECHA ***\n*** TEXTO DE ENTRADA: " + texto + "\n";
		java.util.Calendar cfecha = java.util.Calendar.getInstance();
		cfecha.set(1900, 0, 1);
		java.util.Date fecha = cfecha.getTime();
		String anio = ""; String mes = ""; String dia = "";
		String hora = ""; String minutos = ""; String segundos = "";
		int slash = -1;
		texto = QuitaEspaciosDobles(texto);
		try {
			slash = texto.indexOf("-");
			if (slash < 0) slash = texto.indexOf("/");
			String texto2 = texto.replace("-", "");
			texto2 = texto2.replace("/", "");
			texto2 = texto2.replace(":", "");
			texto2 = texto2.replace(" ", "");
			if (texto.length() >= 8 && ObtieneDouble(texto2) > 0) {
				anio = slash == 2 ? texto2.substring(4, 8) : texto2.substring(0, 4);
				mes = slash == 2 ? texto2.substring(2, 4) : texto2.substring(4, 6);
				dia = slash == 2 ? texto2.substring(0, 2) : texto2.substring(6, 8);
				if (ObtieneInt(mes) >= 1 && ObtieneInt(mes) <= 12 && ObtieneInt(dia) <= 31) {
					log += "*** DIA: " + dia + ", MES: " + mes + ", ANIO: " + anio + "\n";
					hora = texto2.length() > 8 ? texto2.substring(8, 10) : "0";
					minutos = texto2.length() > 8 ? texto2.substring(10, 12) : "0";
					segundos = texto2.length() > 8 ? texto2.substring(12, 14) : "0";
					cfecha.set(Integer.parseInt(anio), Integer.parseInt(mes) - 1, Integer.parseInt(dia),
							   Integer.parseInt(hora), Integer.parseInt(minutos), Integer.parseInt(segundos));
					fecha = cfecha.getTime();
				}
			}
		} catch (Exception e) {
			log += "\n!!! NO ES FECHA: " + texto + ". " + e.getMessage() + "\n";
		} log += "*** FECHA DE SALIDA: " + fecha.toString() + "\n";
		//System.out.println(log);
		cfecha.set(1900, 0, 1);
		if (fecha.equals(cfecha.getTime())) return null;
		else return fecha;
	}
	
	//La variable texto debe tener formato de fecha SQL (aaaa-mm-dd HH:mm:ss), dd/mm/aaaa o mm/dd/aaaa.
	public java.sql.Date ObtieneSQLDate(String texto) {
		String log = "\n*** OBTENER FECHA SQL ***\n*** TEXTO DE ENTRADA: " + texto + "\n";
		java.util.Calendar cfecha = java.util.Calendar.getInstance();
		cfecha.set(1900, 0, 1);
		java.sql.Date fecha = new java.sql.Date(cfecha.getTimeInMillis());
		String anio = ""; String mes = ""; String dia = "";
		String hora = ""; String minutos = ""; String segundos = "";
		int slash = -1;
		texto = QuitaEspaciosDobles(texto);
		try {
			slash = texto.indexOf("-");
			if (slash < 0) slash = texto.indexOf("/");
			String texto2 = texto.replace("-", "");
			texto2 = texto2.replace("/", "");
			texto2 = texto2.replace(":", "");
			texto2 = texto2.replace(" ", "");
			if (texto.length() >= 8 && ObtieneDouble(texto2) > 0) {
				anio = slash == 2 ? texto2.substring(4, 8) : texto2.substring(0, 4);
				mes = slash == 2 ? texto2.substring(2, 4) : texto2.substring(4, 6);
				dia = slash == 2 ? texto2.substring(0, 2) : texto2.substring(6, 8);
				if (ObtieneInt(mes) >= 1 && ObtieneInt(mes) <= 12 && ObtieneInt(dia) >= 1 && ObtieneInt(dia) <= 31) {
					log += "*** DIA: " + dia + ", MES: " + mes + ", ANIO: " + anio + "\n";
					hora = texto2.length() > 8 ? texto2.substring(8, 10) : "0";
					minutos = texto2.length() > 8 ? texto2.substring(10, 12) : "0";
					segundos = texto2.length() > 8 ? texto2.substring(12, 14) : "0";
					cfecha.set(Integer.parseInt(anio), Integer.parseInt(mes) - 1, Integer.parseInt(dia),
							   Integer.parseInt(hora), Integer.parseInt(minutos), Integer.parseInt(segundos));
					fecha = new java.sql.Date(cfecha.getTimeInMillis());
				}
			}
		} catch (Exception e) {
			log += "\n!!! NO ES FECHA: " + texto + ". " + e.getMessage() + "\n";
		} log += "*** FECHA SQL DE SALIDA: " +  fecha.toString() + "\n";
		//System.out.println(log);
		cfecha.set(1900, 0, 1);
		if (fecha.equals(new java.sql.Date(cfecha.getTimeInMillis()))) return null;
		else return fecha;
	}

	public java.util.Date ObtieneDate(java.util.Calendar fecha) {
		//System.out.println("******** " + fecha.getTime());
		java.util.TimeZone.setDefault(java.util.TimeZone.getTimeZone("UTC"));
		java.util.Date d = fecha.getTime();
		//System.out.println("******** " + d);
		return d;
	}
	
	public boolean ObtieneBoolean(String valor) {
		boolean val = false;
		if (!valor.equals("")) {
			if (ObtieneDouble(valor) != 0) val = true;
		} else val = false;
		return val;
	}
	
	public String FormatoFecha(java.util.Date fecha, String formato) {
		java.util.Locale loc = new java.util.Locale("es", "MX");
		java.text.SimpleDateFormat df = new java.text.SimpleDateFormat(formato, loc);  
		String sfecha = "";
		try {
			sfecha = df.format(fecha);
		} catch (Exception e) {
			System.out.println("\n!!!NO SE OBTUVO LA FECHA POR LO SIGUIENTE: " + e.getMessage());
		} return sfecha;
	}

	public String FormatoFecha(java.util.Calendar fecha, String formato) {
		java.util.Locale loc = new java.util.Locale("es", "MX");
		java.text.SimpleDateFormat df = new java.text.SimpleDateFormat(formato, loc);  
		String sfecha = "";
		try {
			sfecha = df.format(fecha.getTime());
		} catch (Exception e) {
			System.out.println("\n!!!NO SE OBTUVO LA FECHA POR LO SIGUIENTE: " + e.getMessage());
		} return sfecha;
	}
	
	public String FormatoNumero(int numero) {
		java.util.Locale loc = new java.util.Locale("es", "MX");
		java.text.NumberFormat nf = java.text.NumberFormat.getInstance(loc);
		nf.setMinimumFractionDigits(0); nf.setMaximumFractionDigits(0);
		String sNumero = "";
		try {
			sNumero = nf.format(numero);
		} catch (Exception e) {
			System.out.println("\n!!! NO SE PUEDE FORMATEAR EL NUMERO " + numero + 
							   " POR LO SIGUIENTE " + e.getMessage());
		} return sNumero;
	}

	public String FormatoNumero(long numero) {
		java.util.Locale loc = new java.util.Locale("es", "MX");
		java.text.NumberFormat nf = java.text.NumberFormat.getInstance(loc);
		nf.setMinimumFractionDigits(0); nf.setMaximumFractionDigits(0);
		String sNumero = "";
		try {
			sNumero = nf.format(numero);
		} catch (Exception e) {
			System.out.println("\n!!! NO SE PUEDE FORMATEAR EL NUMERO " + numero + 
					   " POR LO SIGUIENTE " + e.getMessage());
		} return sNumero;
	}
	
	public String FormatoNumero(double numero, int digitos) {
		java.util.Locale loc = new java.util.Locale("es", "MX");
		java.text.NumberFormat nf = java.text.NumberFormat.getNumberInstance(loc);
		java.text.DecimalFormat df = (java.text.DecimalFormat) nf;
		String ceros = "";
		for (int i = 0; i < digitos; i++) ceros += (ceros == "" ? "." : "") + "0";
		df.applyPattern("###,###,###,##0" + ceros);
		String sNumero = "";
		try {
			sNumero = df.format(numero);
		} catch (Exception e) {
			System.out.println("\n!!! NO SE PUEDE FORMATEAR EL NUMERO " + numero + 
							   " POR LO SIGUIENTE " + e.getMessage());
		} return sNumero;
	}

	/*public String FormatoNumero(double numero, int digitos) {
		java.util.Locale loc = new java.util.Locale("es", "MX");
		java.text.NumberFormat nf = java.text.NumberFormat.getInstance(loc);
		nf.setMinimumFractionDigits(digitos); nf.setMaximumFractionDigits(digitos);
		String sNumero = "";
		try {
			sNumero = nf.format(numero);
		} catch (Exception e) {
			System.out.println("\n!!! NO SE PUEDE FORMATEAR EL NUMERO " + numero + 
							   " POR LO SIGUIENTE " + e.getMessage());
		} return sNumero;
	}*/

	public String FormatoNumeroEntero(int numero, int digitos) {
		java.util.Locale loc = new java.util.Locale("es", "MX");
		java.text.NumberFormat nf = java.text.NumberFormat.getInstance(loc);
		nf.setMinimumIntegerDigits(digitos); nf.setMaximumIntegerDigits(digitos);
		String sNumero = ""; String ceros = "";
		try {
			for (int i = 1; i <= digitos; i++) ceros += "0";
			sNumero = ceros + numero;
			sNumero = sNumero.substring(sNumero.length() - digitos, sNumero.length());
			//sNumero = nf.format(numero);
		} catch (Exception e) {
			System.out.println("\n!!! NO SE PUEDE FORMATEAR EL NUMERO " + numero + 
							   " POR LO SIGUIENTE " + e.getMessage());
		} return sNumero;
	}
	
	public String FormatoNumeroEntero(long numero, int digitos) {
		java.util.Locale loc = new java.util.Locale("es", "MX");
		java.text.NumberFormat nf = java.text.NumberFormat.getInstance(loc);
		nf.setMinimumIntegerDigits(digitos); nf.setMaximumIntegerDigits(digitos);
		String sNumero = ""; String ceros = "";
		try {
			for (int i = 1; i <= digitos; i++) ceros += "0";
			sNumero = ceros + numero;
			sNumero = sNumero.substring(sNumero.length() - digitos, sNumero.length());
			//sNumero = nf.format(numero);
		} catch (Exception e) {
			System.out.println("\n!!! NO SE PUEDE FORMATEAR EL NUMERO " + numero + 
							   " POR LO SIGUIENTE " + e.getMessage());
		} return sNumero;
	}

	public String FormatoNumeroDecimales(double numero, int digitos) {
		java.util.Locale loc = new java.util.Locale("es", "MX");
		java.text.NumberFormat nf = java.text.NumberFormat.getInstance(loc);
		nf.setMinimumIntegerDigits(digitos); nf.setMaximumIntegerDigits(digitos);
		String sNumero = ""; String ceros = "";
		try {
			for (int i = 1; i <= digitos; i++) ceros += "0";
			sNumero = numero + ceros; int punto = sNumero.indexOf(".") + 1;
			sNumero = sNumero.substring(0, punto + digitos);
			//sNumero = nf.format(numero);
		} catch (Exception e) {
			System.out.println("\n!!! NO SE PUEDE FORMATEAR EL NUMERO " + numero + 
							   " POR LO SIGUIENTE " + e.getMessage());
		} return sNumero;
	}

	
	public void agregaParametro(String parametro) { this.parametros += parametro + (parametros == "" ? "" : "|"); }
	public void agregaParametro(int parametro) { this.parametros += parametro + (parametros == "" ? "" : "|"); }
	public void agregaParametro(long parametro) { this.parametros += parametro + (parametros == "" ? "" : "|"); }
	public void agregaParametro(double parametro) { this.parametros += parametro + (parametros == "" ? "" : "|"); }
	public void agregaParametro(Object parametro) { this.parametros += parametro.toString() + (parametros == "" ? "" : "|"); }
	
	public void agregaParametro(String... parametro) {
		for (int i = 0; i < parametro.length; i++)
			this.parametros += parametro[i] + (parametros == "" ? "" : parametro.length == 1 ? "" : "|");
	}

	public String obtenerParametro(String texto, int posicion, String separador) {
		String valor = ""; int ini = 0; int fin = 0;
		int cont = 0; int largo = 0;
		if (QuitaEspaciosDobles(texto) != "") {
			while (cont < posicion && ini <= texto.length()) {
				fin = texto.indexOf(separador, ini);
				if (fin < 0) fin = texto.length();
				valor = texto.substring(ini, fin);
				ini = fin + 1; cont++;
			} if (cont < posicion) valor = "";
		} System.out.println("***** PARAMETRO " + posicion + ": " + valor);
		return valor;
	}
	
	/*
	public String asignaParametro(String texto, String valor, int posicion, String separador) {
		valor = QuitaEspaciosDobles(valor); int ini = 0; int fin = 0;
		System.out.println("**** ENTRADA: " + texto);
		if (QuitaEspaciosDobles(texto) != "") {
			while (texto.indexOf(separador, ini) >= 0 && posicion > 0) {
				fin = texto.indexOf(separador, ini); posicion--;
				if (posicion > 0) ini = fin + 1;
			} if (ini <= fin && fin > 0) {
				texto = texto.substring(0, ini) + valor + texto.substring(fin, texto.length());
			} else {
				posicion--;
				if (posicion == 0) texto = texto.substring(0, ini) + valor;
			}
		} System.out.println("**** SALIDA: " + texto);
		return texto;
	}*/
	
	public String asignaParametro(String texto, String valor, int posicion, String separador) {
		valor = QuitaEspaciosDobles(valor);
		int	inicio = 0; int	fin = 0; int largo = 0;
		String texto_nuevo = "";
		int	cont = 0;

		//System.out.println("**** ENTRADA: " + texto);
		if (!QuitaEspaciosDobles(texto).equals("")) {

			//Control para evitar SQL injection
			if	(valor.indexOf("select") >= 0 || valor.indexOf("insert") > 0 ||
				 valor.indexOf("update") > 0 || valor.indexOf("delete") > 0 ||
				 valor.indexOf("create") > 0 || valor.indexOf("alter") > 0 ||
				 valor.indexOf("drop") > 0 || valor.indexOf("exec") > 0)
				valor = "";
			
			fin = texto.indexOf(separador, inicio);
			while (fin >= 0) {
				cont++;
				if (cont == posicion) texto_nuevo += valor + separador;
				else {
					largo = fin - inicio;
					texto_nuevo += texto.substring(inicio, inicio + largo) + separador;
				}
				inicio = fin + 1;
				fin = texto.indexOf(separador, inicio);
			} cont++; if (cont == posicion) texto_nuevo += valor;
			else texto_nuevo = texto_nuevo + texto.substring(inicio, texto.length());
		}
		//System.out.println("**** SALIDA: " + texto_nuevo);
		return texto_nuevo;
	}
	
	//Llena el objeto de mensajes de excepci�n con los datos indicados
	/*public Mensaje_Excepcion setMensaje_Excepcion(int codigo, String mensaje) {
		Mensaje_Excepcion mensajeExcepcion = new Mensaje_Excepcion();
		mensajeExcepcion.setCodigo(codigo);
		mensajeExcepcion.setMensaje(mensaje);
		return mensajeExcepcion;
	}*/
	
	public boolean EnviaCorreo(String hostSmtp, String portSmtp, final String user, final String password,
							   String senderAddress, String toAddress, String ccAddress, String bccAddress,
							   String subject, String mensaje) {

		boolean blnVal = false;
		boolean isHTMLFormat = true;
		boolean debug = false;

		Properties properties = new Properties();
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.host", hostSmtp);
		properties.put("mail.smtp.port", portSmtp);

		Authenticator myAuth = new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user, password);
			}
		};

		Session ses = Session.getInstance(properties, myAuth);
		ses.setDebug(debug);

		try {
			MimeMessage msg = new MimeMessage(ses);

			// Remitente.
			msg.setFrom(new InternetAddress(senderAddress));
			System.out.println("De: " + senderAddress);

			// Destinatarios.
			if (!QuitaEspaciosDobles(toAddress).equals("")) {
				msg.setRecipients(Message.RecipientType.TO, toAddress);
				System.out.println("Para: " + toAddress);
			}

			// CC (con copia).
			if (!QuitaEspaciosDobles(ccAddress).equals("")) {
				msg.setRecipients(Message.RecipientType.CC, ccAddress);
				System.out.println("CC: " + ccAddress);
			}

			// BCC (con copia oculta).
			if (!QuitaEspaciosDobles(bccAddress).equals("")) {
				msg.setRecipients(Message.RecipientType.BCC, bccAddress);
				System.out.println("BCC: " + bccAddress);
			}

			// Asunto.
			msg.setSubject(subject);
			System.out.println("Asunto: " + subject);

			// Fecha.
			msg.setSentDate(new java.util.Date());

			// Mensaje.
			// msg.setText(mensaje, "UTF-8");

			MimeMultipart multipart = new MimeMultipart();
			MimeBodyPart mbp = new MimeBodyPart();

			if (isHTMLFormat) {
				mbp.setContent(mensaje, "text/html");
				System.out.println("Mensaje: " + mensaje);
			} else {
				mbp.setText(mensaje, "UTF-8");
				System.out.println("Mensaje: " + mensaje);
			}

			multipart.addBodyPart(mbp);
			msg.setContent(multipart);

			try {
				Transport.send(msg);
				blnVal = true;
			} catch (Exception e) {
				e.printStackTrace();
				System.out
						.println("!!! NO SE ENVIO EL MENSAJE POR LO SIGUIENTE: "
								+ e.getMessage());
				blnVal = false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("!!! NO SE ENVIO EL MENSAJE POR LO SIGUIENTE: "
					+ e.getMessage());
			blnVal = false;
		}

		return blnVal;
	}
	
	public boolean EnviaCorreo(String hostSmtp, String portSmtp, final String user, final String password,
			   				   String senderAddress, String senderAddressName, String toAddress, String ccAddress,
			   				   String bccAddress, String subject, String mensaje) {
		if (!QuitaEspaciosDobles(senderAddressName).equals("")) {
			senderAddress = QuitaEspaciosDobles(senderAddressName) + " <" + QuitaEspaciosDobles(senderAddress) + ">";
		}
		return
			EnviaCorreo(hostSmtp, portSmtp, user, password, senderAddress, toAddress, ccAddress, bccAddress,
					    subject, mensaje);
	}
}
