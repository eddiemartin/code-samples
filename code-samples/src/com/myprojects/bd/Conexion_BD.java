package com.myprojects.bd;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.myprojects.utils.Utilerias;

public class Conexion_BD {
	Utilerias util = new Utilerias();
	public int regs = 0;
	
	final String SEPARADOR = "##";
	final int LIMITE = 999;
	
	public String Server = "";
	public String DataBase = "";
	
	public Conexion_BD() {}

	public Connection abrirConexion(String driver, String dataBase,
								    String user, String password) throws SQLException {
		Connection connection = null; String log = "\n";
		try {
			log += "*** ESTABLECIENDO CONEXION CON LA BASE DE DATOS " + dataBase + " ***\n";
			Class.forName(driver); 
			connection = DriverManager.getConnection(dataBase, user, password);
			if (!connection.isClosed()) {
				log += "*** CONEXION CON LA BASE DE DATOS " + dataBase + " ESTABLECIDA ***\n";
				this.DataBase = connection.getCatalog();
				this.Server = connection.getMetaData().getURL();
			} else log += "*** NO SE ESTABLECIO CONEXION CON LA BASE DE DATOS " + dataBase + " ***\n";
		} catch (Exception e) {
			log += "!!! ERROR AL CONECTAR CON LA BASE DE DATOS: " + e.getMessage() + "\n"; 
			e.printStackTrace();
		}
		System.out.println(log);
		System.out.println(this.DataBase);
		System.out.println(this.Server);
		return connection;
	}

	public Connection abrirConexion(String archivo) throws SQLException {
		ResourceBundle rb = ResourceBundle.getBundle(archivo);
		Connection connection = null;
		try {
			connection = abrirConexion(rb.getString("conexion_drv"), rb.getString("conexion_dtb"), 
						 			   rb.getString("conexion_usr"), rb.getString("conexion_pwd"));
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return connection;
	}
		
	public Connection abrirPoolConexion(String pool) throws SQLException {
		Connection connection = null; String log = "\n";
		try {
			log += "*** ESTABLECIENDO CONEXION CON EL RECURSO " + pool + " ***\n";
			Context ctx = new InitialContext();
			DataSource ds=(DataSource) ctx.lookup(pool);
			connection = ds.getConnection();
			if (!connection.isClosed()) {
				log += "*** CONEXION CON EL RECURSO " + pool + " ESTABLECIDA ***\n";
				this.DataBase = connection.getCatalog();
				this.Server = connection.getMetaData().getURL();
			} else log += "*** NO SE ESTABLECIO CONEXION CON EL RECURSO " + pool + " ***\n";
		} catch (Exception e) {
			log += "!!! ERROR AL CONECTAR CON EL RECURSO: " + e.getMessage() + "\n"; 
			//e.printStackTrace();
		}
		System.out.println(log);
		System.out.println(this.DataBase);
		System.out.println(this.Server);
		return connection;
	}
	
	public ResultSet obtenerDatos(Connection con, String query) throws SQLException {
		ResultSet rsDatos = null; String log = "\n";
		log += "*** GENERANDO CONSULTA: " + query + "\n"; 
		try {
			if (con != null) {
				con.setAutoCommit(true);
				Statement st = con.createStatement();
				rsDatos = st.executeQuery(query);
			} log += "*** CONSULTA TERMINADA." + (rsDatos == null ? " NO HAY DATOS." : "") + "\n";
		} catch (Exception e) {
			log += "!!! ERROR AL GENERAR LA CONSULTA: " + e.getMessage() + "\n";
			rsDatos = null;
		}
		System.out.println(log);
		return rsDatos;
	}

	public ResultSet obtenerDatos(Connection con, String sp, String sptypes, String params) throws SQLException {
		Calendar t0 = Calendar.getInstance();
		ResultSet rsDatos = null; String log = "\n";
		log += "*** GENERANDO CONSULTA: " + sp + "\n"; 
		try {
			if (con != null) {
				con.setAutoCommit(true);
				//log += "*** ENTRADA: " + params + "\n";
				String[] pr = params.split(SEPARADOR, LIMITE);
				log += "*** NO. PARAMETROS: " + pr.length + "\n";
				String[] type = sptypes.split(","); 
				CallableStatement st = con.prepareCall(sp, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				log += "*** PARAMETROS: " + params + "\n";
				for (int i = 0; i < pr.length; i++) {
					log += "*** PARAMETRO " + (i + 1) + ": " + pr[i] + "\n";
					String val = util.QuitaEspaciosDobles(pr[i]);
					if (type[i].equals("string")) st.setString(i + 1, val);
					if (type[i].equals("int")) st.setInt(i + 1, Integer.parseInt(val));
					if (type[i].equals("double")) st.setDouble(i + 1, Double.parseDouble(val));
					if (type[i].equals("long")) st.setLong(i + 1, Long.parseLong(val));
					if (type[i].equals("date")) st.setDate(i + 1, util.ObtieneSQLDate(val));
					if (type[i].equals("boolean")) st.setBoolean(i + 1, util.ObtieneBoolean(val));
				}
				st.executeQuery();
				rsDatos = st.getResultSet();
			} log += "\n*** CONSULTA TERMINADA." + (rsDatos == null ? " NO HAY DATOS." : "") + "\n";
		} catch (Exception e) {
			log += "\n!!! ERROR AL GENERAR LA CONSULTA: " + e.getMessage() + "\n";
			e.printStackTrace();
			rsDatos = null;
		} finally {
			Calendar t1 = Calendar.getInstance();
			double tiempo = (t1.getTimeInMillis() - t0.getTimeInMillis()) / 1000.0;
			log += "*** TIEMPO DE CONSULTA: " + tiempo + " SEGUNDOS.\n";
		}	
		System.out.println(log);
		return rsDatos;
	}

	public boolean ejecutar(String archivo, String[] sp, String sptypes, String[] params) throws SQLException {
		boolean blnVal = false; String log = "\n";
		Connection con = null;
		try {
			ResourceBundle rb = ResourceBundle.getBundle(archivo);
			String pool = rb.getString("conexion_pool");
			con = this.abrirPoolConexion(pool);
			if (con == null) con = abrirConexion(archivo);
			if (con != null) {
				con.setAutoCommit(false);
				//log += "*** ENTRADA: " + params + "\n";
				for (int j = 0; j < (sp.length >= params.length ? sp.length : params.length); j++) {
					log += "*** EJECUTANDO SENTENCIA: " + sp[j] + "\n";
					String[] pr = params[j].split(SEPARADOR, LIMITE);
					log += "*** NO. PARAMETROS: " + pr.length + "\n";
					String[] type = sptypes.split(","); 
					CallableStatement st = con.prepareCall(sp[j]);
					log += "*** PARAMETROS: " + params + "\n";
					for (int i = 0; i < pr.length; i++) {
						log += "*** PARAMETRO " + (i + 1) + ": " + pr[i] + "\n";
						String val = util.QuitaEspaciosDobles(pr[i]);
						if (type[i].equals("string")) st.setString(i + 1, val);
						if (type[i].equals("int")) st.setInt(i + 1, Integer.parseInt(val));
						if (type[i].equals("double")) st.setDouble(i + 1, Double.parseDouble(val));
						if (type[i].equals("long")) st.setLong(i + 1, Long.parseLong(val));
						if (type[i].equals("date")) st.setDate(i + 1, util.ObtieneSQLDate(val));
						if (type[i].equals("boolean")) st.setBoolean(i + 1, util.ObtieneBoolean(val));
					} st.execute();
					log += "*** SENTENCIA TERMINADA.\n";
				} con.commit();
				blnVal = true;
			} 
		} catch (Exception e) {
			log += "!!! ERROR AL EJECUTAR LA SENTENCIA: " + e.getMessage() + "\n";
			e.printStackTrace();
			if (!con.getAutoCommit()) con.rollback();
		} finally {
			con.setAutoCommit(true);
		}
		System.out.println(log);
		return blnVal;
	}
	
	public boolean ejecutar(String archivo, String sp, String sptypes, String params) throws SQLException {
		boolean blnVal = false; String log = "\n";
		log += "*** EJECUTANDO SENTENCIA: " + sp + "\n";
		Connection con = null;
		try {
			ResourceBundle rb = ResourceBundle.getBundle(archivo);
			String pool = rb.getString("conexion_pool");
			con = this.abrirPoolConexion(pool);
			if (con == null) con = abrirConexion(archivo);
			if (con != null) {
				con.setAutoCommit(false);
				//log += "*** ENTRADA: " + params + "\n";
				String[] pr = params.split(SEPARADOR, LIMITE);
				log += "*** NO. PARAMETROS: " + pr.length + "\n";
				String[] type = sptypes.split(","); 
				CallableStatement st = con.prepareCall(sp);
				log += "*** PARAMETROS: " + params + "\n";
				for (int i = 0; i < pr.length; i++) {
					log += "*** PARAMETRO " + (i + 1) + ": " + pr[i] + "\n";
					String val = util.QuitaEspaciosDobles(pr[i]);
					if (type[i].equals("string")) st.setString(i + 1, val);
					if (type[i].equals("int")) st.setInt(i + 1, Integer.parseInt(val));
					if (type[i].equals("double")) st.setDouble(i + 1, Double.parseDouble(val));
					if (type[i].equals("long")) st.setLong(i + 1, Long.parseLong(val));
					if (type[i].equals("date")) st.setDate(i + 1, util.ObtieneSQLDate(val));
					if (type[i].equals("boolean")) st.setBoolean(i + 1, util.ObtieneBoolean(val));
				} st.execute();
				log += "*** SENTENCIA TERMINADA.\n";
				con.commit();
				blnVal = true;
			} 
		} catch (Exception e) {
			log += "!!! ERROR AL EJECUTAR LA SENTENCIA: " + e.getMessage() + "\n";
			e.printStackTrace();
			if (!con.getAutoCommit()) con.rollback();
		} finally {
			con.setAutoCommit(true);
		}
		System.out.println(log);
		return blnVal;
	}
	
	public boolean cerrarConexion(Connection con) {
		boolean blnVal = false; String log = "";
		try {
			String conexion = con.getCatalog();
			if (!con.isClosed()) con.close();
			blnVal = true;
			log = "\n*** CONEXION A BASE DE DATOS " + conexion + " CERRADA.";
		} catch (SQLException e) {
			e.printStackTrace();
			log = "\n!!! NO SE CERRO ADECUADAMENTE LA CONEXION A BASE DE DATOS POR LO SIGUIENTE: " + e.getMessage();
		} finally {
			con = null;
		} System.out.println(log);
		return blnVal;
	}
	
	@SuppressWarnings({ "rawtypes"})
	public ArrayList obtenerDatos(Object objeto, String nombre_consulta, String parametros) {
		return  obtenerDatos(objeto, nombre_consulta, parametros, 0, 0, "");
	}

	@SuppressWarnings({ "rawtypes"})
	public ArrayList obtenerDatos(Object objeto, String nombre_consulta, String parametros, String archivo) {
		return  obtenerDatos(objeto, nombre_consulta, parametros, 0, 0, archivo);
	}

	@SuppressWarnings({ "rawtypes" })
	public ArrayList obtenerDatos(Object objeto, String nombre_consulta, String parametros, int tamanio, int registro) {
		return  obtenerDatos(objeto, nombre_consulta, parametros, tamanio, registro, "");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ArrayList obtenerDatos(Object objeto, String nombre_consulta, String parametros, int tamanio, int registro,
							      String archivo) {
		Calendar t0 = Calendar.getInstance();
		ResourceBundle rb = ResourceBundle.getBundle(archivo);
		String sp = rb.getString(nombre_consulta);
		String sptypes = rb.getString(nombre_consulta + "_tipos");
		String pool = rb.getString("conexion_pool");
		String log = "";
		ResultSet rsDatos = null;
		Class<?> cu = objeto.getClass();
		ArrayList lista = new java.util.ArrayList();
		try {
			Connection con = null;
			con = this.abrirPoolConexion(pool);
			if (con == null) con = abrirConexion(archivo);
			rsDatos = this.obtenerDatos(con, sp, sptypes, parametros);
			Calendar t1 = Calendar.getInstance();
			double tiempo = (t1.getTimeInMillis() - t0.getTimeInMillis()) / 1000.0;
			log += "\n*** TIEMPO TOTAL DE CONSULTA: " + tiempo + " SEGUNDOS.";
			if (rsDatos != null) {
				rsDatos.last();
				int registros = rsDatos.getRow();
				if (registros > 0) {
					int cont = 0;
					log += "\n*** LLENANDO OBJETO TIPO " + cu.getName();
					int cols = rsDatos.getMetaData().getColumnCount();
					java.lang.reflect.Field[] campos = cu.getDeclaredFields();
					if (registro > registros) registro = registros - tamanio; 
					if (registro < 0) registro = 0;
					while (registro <= registros && (tamanio <= 0 || tamanio > 0 && cont < tamanio)) {
						if (rsDatos.absolute(registro++)) {
							Object objeto2 = cu.newInstance();
							for (int i = 0; i < (cols >= campos.length ? campos.length : cols); i++) {
								try {
									campos[i].setAccessible(true);
									String tipo = campos[i].getType().toString();
									String val = util.QuitaEspaciosDobles(rsDatos.getString(i + 1));
									if (tipo.indexOf("java.util.Date") >= 0)
										campos[i].set(objeto2, util.ObtieneDate(val));
									else if (tipo.indexOf("java.sql.Date") >= 0)
										campos[i].set(objeto2, util.ObtieneSQLDate(val));
									else if (tipo.indexOf("String") >= 0)
										campos[i].set(objeto2, val);
									else if (tipo.indexOf("int") >= 0)
										campos[i].set(objeto2, util.ObtieneInt(val));
									else if (tipo.indexOf("long") >= 0)
										campos[i].set(objeto2, util.ObtieneLong(val));
									else if (tipo.indexOf("double") >= 0)
										campos[i].set(objeto2,util.ObtieneDouble(val));
									else if (tipo.indexOf("boolean") >= 0)
										campos[i].set(objeto2,util.ObtieneBoolean(val));
									else campos[i].set(objeto2, (Object) val);
									//log += "\n    " + campos[i].getName() + " = " + val;
								} catch (IllegalArgumentException e) {
									log += "\n!!! NO SE LLENO CORRECTAMENTE EL OBJETO POR LO SIGUIENTE: " + e.getMessage();
									e.printStackTrace();
								} catch (IllegalAccessException e) {
									log += "\n!!! NO SE LLENO CORRECTAMENTE EL OBJETO POR LO SIGUIENTE: " + e.getMessage();
									e.printStackTrace();
								}
							} lista.add(objeto2);
						} cont++;
					} log += "\n*** REGISTROS EN EL OBJETO: " + lista.size();
					rsDatos.close();
					regs = registros;
					Calendar t2 = Calendar.getInstance();
					tiempo = (t2.getTimeInMillis() - t1.getTimeInMillis()) / 1000.0;
					log += "\n*** TIEMPO DE LLENADO DEL OBJETO: " + tiempo + " SEGUNDOS.";
				} else log += "\n*** OBJETO VACIO.";
			} this.cerrarConexion(con);
		} catch (Exception e) { e.printStackTrace(); }
		finally {
			Calendar t1 = Calendar.getInstance();
			double tiempo = (t1.getTimeInMillis() - t0.getTimeInMillis()) / 1000.0;
			log += "\n*** TIEMPO TOTAL DE EJECUCION: " + tiempo + " SEGUNDOS.";
			System.out.println(log);
		} return lista;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object[] obtenerDatos(Object[] objeto, String nombre_consulta, String parametros, String archivo) {
		try {
			Class<?> cu = objeto[0].getClass(); Object o = cu.newInstance();
			ArrayList lista = obtenerDatos(o, nombre_consulta, parametros, 0, 0, archivo);
			if (!lista.isEmpty()) { objeto = new Object[lista.size()]; lista.toArray(objeto); }
			else objeto = null;
		} catch (Exception e) { e.printStackTrace(); objeto = null; }
		return objeto;
	}
}
